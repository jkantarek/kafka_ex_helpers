defmodule KafkaExHelpers.Mixfile do
  use Mix.Project

  def project do
    [app: :kafka_ex_helpers,
     version: "0.1.2",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(),

     package: [
       description: "An unofficial set of helper utilities for KafkaEx",
       licenses: ["GNUV3"],
       maintainers: ["Jeff Kantarek"],
       links: %{gitlab: "https://gitlab.com/jkantarek/kafka_ex_helpers"},
     ],

     # Docs
     name: "KafkaExHelpers",
     source_url: "https://gitlab.com/jkantarek/kafka_ex_helpers",
     homepage_url: "https://jkantarek.gitlab.io/kafka_ex_helpers/readme.html",
     docs: [
       main: "readme", # The main page in the docs
       extras: ["README.md"],
       output: "public" #folder required for github pages docs
     ]
   ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:poison, "~> 2.0"},
      {:credo, "~> 0.5", only: [:dev, :test]},
      {:kafka_ex, "~> 0.6"},
      {:uuid, "~> 1.1" },
      {:ex_doc, "~> 0.14", only: :dev},
      {:dialyxir, "~> 0.4", only: [:dev, :test], runtime: false},
      {:flow, "~> 0.11"}
    ]
  end
end
